export function calculator(operator, num1, num2) {
  if (typeof num1 !== "number" || typeof num2 !== "number") {
    return NaN;
  }
  switch(operator) {
  case "+":
    return num1 + num2;
    // break;
  case "-":
    return num1 - num2;
  case "*":
    return num1 * num2;
  case "/":
    if (num2 === 0) {
      throw new Error("Cannot divide with 0");
    }
    return num1 / num2;
  default:
    return NaN;
  }
}

// console.log(calculator("+", -6, 16));
// console.log(calculator("-", 17, 12));
// console.log(calculator("*", 4, 5));
// console.log(calculator("/", -18, 4));
// console.log(calculator("**", 2, 3));